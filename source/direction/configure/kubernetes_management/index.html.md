---
layout: markdown_page
title: "Category Direction - Kubernetes Management"
---

- TOC
{:toc}

## Kubernetes Management

Configuring and managing your Kubernetes clusters can be a complex, time-consuming task. We aim to provide a simple way for users to configure their clusters within GitLab; tasks such as scaling, adding, and deleting clusters become simple, single-click events.

Related grab-bad epic [gitlab.com/groups/gitlab-org/-/epics/253](https://gitlab.com/groups/gitlab-org/-/epics/253)


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=kubernetes)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/115) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

In order to make the applications provided as part of GitLab-managed apps customizable, we are evolving the Kubernetes integration to manage all install/upgrade/uninstall actions via GitLab CI. This will provide not only the ability to customize applications, but will also provide version control, security, and all the GitLab features we know and love. The next step in this journey is to create CI templates for all the applications we support today.

[Create CI templates for GitLab-managed-apps](https://gitlab.com/groups/gitlab-org/-/epics/2103#course-of-action)

These features will take our Kubernetes integration to [viable maturity](https://gitlab.com/groups/gitlab-org/-/epics/1334).

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is [Complete](/direction/maturity/). 

## Challenges

GitLab is the most popular way to deploy to Kubernetes. Many organizations (of all scales) have adopted Kubernetes prior 
to their use of our GitLab. We will support this model by identifying the most common set of use cases where Kubernetes 
clusters exist. We'll then enable those existing clusters to be plugged in to GitLab so users can gain the value of using 
our integrated development approach. This includes both enterprise users of Kubernetes (typically OpenShift) and startup 
Kubernetes use (typically public cloud providers).

Our Kubernetes integration currently operates under the assumption of `cluster-admin` privilege. While this is acceptable 
for most use cases, there is a large minority that, appropriately, operates strictly on least-privilege security 
principle. In these environments only single-namespace access is allowed and cluster-wide access is denyed. To encourage 
adoption of these principles, we will provide the same rich integration we provide today for this further restricted use 
case.

## Competitive landscape

### Kubespray
Kubespray provides a set of Ansible roles for Kubernetes deployment and configuration. Kubespray can use AWS, GCE, Azure, OpenStack or a bare metal Infrastructure as a Service (IaaS) platform. Kubespray is an open-source project with an open development model. The tool is a good choice for people who already know Ansible as there’s no need to use another tool for provisioning and orchestration. Kubespray uses kubeadm under the hood.

### Kubeadm
Kubeadm is a Kubernetes distribution tool since version 1.4. The tool helps to bootstrap best-practice Kubernetes clusters on existing infrastructure. Kubeadm cannot provision infrastructure for you though. Its main advantage is the ability to launch minimum viable Kubernetes clusters anywhere. Add-ons and networking setup are both out of Kubeadm’s scope though, so you will need to install this manually or using another tool.

### Kops
Kops helps you create, destroy, upgrade, and maintain production-grade, highly available Kubernetes clusters from the command line. Amazon Web Services (AWS) is currently officially supported, with GCE in beta support, and VMware vSphere in alpha, and other platform support is planned. Kops allows you to control the full Kubernetes cluster lifecycle; from infrastructure provisioning to cluster deletion.

### Conjure-up
Conjure-up is a Canonical product which allows you to deploy “The Canonical Distribution of Kubernetes on Ubuntu” with a few simple commands. It supports AWS, GCE, Azure, Joyent, OpenStack, VMware, bare metal, and localhost deployments. Juju, MAAS, and LXD are the underlying technology for Conjure-up.

## Analyst landscape

This category doesn't quite fit the "configuration management" area as it relates only to Kubernetes. No analyst area currently defined.

## Top Customer Success/Sales issue(s)

[Test Kubernetes and Auto DevOps features on OpenShift](https://gitlab.com/gitlab-org/gitlab-ce/issues/44669)

## Top user issue(s)

[Installing Helm Tiller on Kubernetes via gitlab UI is impossible behind the proxy](https://gitlab.com/gitlab-org/gitlab-ce/issues/46366)

## Top internal customer issue(s)

[Allow advanced configuration of GitLab runner when installing via helm chart](https://gitlab.com/gitlab-org/gitlab-ce/issues/55885)

## Top Vision Item(s) 

[Allow user to uninstall gitlab managed apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/49408)
